This repo uses the cert-utils-operator to inject certs from tls secrets into the routes. This operator will also issue alarms in OCP when the certs are about to reach EOL.

https://github.com/redhat-cop/cert-utils-operator

The tls secrets should be encrypted as sealed secrets.

https://sealed-secrets.netlify.app/

bootstrap/ contains the code needed to add this repo to ArgoCD.
